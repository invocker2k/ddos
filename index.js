require("dotenv").config();
// const environment = process.env;
const PORT = process.env.PORT || 9999;
const express = require("express");
const _ = require("lodash");
const routes = require("./routes");
const app = express();
const bodyParser = require("body-parser");
app.use(
  bodyParser.urlencoded({
    extended: true,
    limit: "100mb",
    parameterLimit: 50000,
  })
);

app.use(bodyParser.json({ limit: "100mb" }));

// const links = [
//   "https://chuyenmucit.code.blog/",
//   "https://chuyenmucit.code.blog/",
// ];

// const num = 10;

function init() {
  app.route("/test").get((req, res) => {
    res.status(200).send(`helo iam in port:+ ${PORT}`);
  });
  routes(app);
  app.listen(PORT, () => {
    console.log("post:", PORT);
  });
}

init();
