const axios = require("axios");
const hengio = require("node-schedule");
let count = 0;
const _ = require("lodash");

let job;
module.exports = (app) => {
  app.route("/scraper").get((req, res) => {
    const links = req.body.links;
    const num = req.body.flow;
    if (!links || typeof links !== "object")
      return res.status(412).json({ err: "no links" });

    job = hengio.scheduleJob(" */1 * * * * *", () => {
      _.map(links, (link) => {
        for (let i = 0; i < num; i++) {
          axios.get(link).then((x) => console.log(x));
          count += 1;
          console.log(count);
        }
      });
    });
    return res.status(200).json({ status: "hack ok" });
  });

  app.route("/stop-job").get((req, res) => {
    try {
      const isCancel = job.cancel();
      return isCancel
        ? res.sendStatus(200).json({ status: "cancelok" })
        : res.sendStatus(200).json({ status: "cancel false" });
    } catch {
      (err) => {
        console.log("?");
        return res.sendStatus(200).json({ status: "cancel false", err });
      };
    }
  });
  app.route("/info-job").get((req, res) => {
    res.status(200).json({ counter: count });
  });
};
